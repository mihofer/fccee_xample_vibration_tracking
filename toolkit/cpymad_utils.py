
import toolkit.create_error_table as cet


def set_up_lattice(mad_instance, reference_parameter, seq_path):

    mad_instance.call(seq_path)
    mad_instance.beam(
                particle='electron',
                npart=reference_parameter['BUNCH_POPULATION'],
                kbunch=reference_parameter['BUNCHES'],
                pc=reference_parameter['ENERGY'],
                radiate=False,
                bv=1,
                ex=reference_parameter['EMITTANCE_X'],
                ey=reference_parameter['EMITTANCE_Y'])
    mad_instance.use('FCCEE_P_RING')


def match_chromaticity(mad_instance, chromas, sextupole_script_path):
    mad_instance.call(sextupole_script_path)
    mad_instance.input(
f"""MATCH, SEQUENCE=FCCEE_P_RING, CHROM;
VARY, NAME=KN_SF;
VARY, NAME=KN_SD;

GLOBAL, DQ1={chromas['QX']};
GLOBAL, DQ2={chromas['QY']};

LMDIF, CALLS=10000;
ENDMATCH;
""")


def slice_lattice(mad_instance):

    mad_instance.input(
"""SELECT, FLAG=makethin, CLASS=RFCAVITY, SLICE = 1;
SELECT, FLAG=makethin, CLASS=rbend, SLICE = 10;
SELECT, FLAG=makethin, CLASS=quadrupole, SLICE = 10;
SELECT, FLAG=makethin, CLASS=sextupole, SLICE = 10;
        
SELECT, FLAG=makethin, PATTERN="^QC.*", SLICE=20;
SELECT, FLAG=makethin, PATTERN="^SY.*", SLICE=20;
        
MAKETHIN, SEQUENCE=FCCEE_P_RING, STYLE=TEAPOT, MAKEDIPEDGE=False;""")
    mad_instance.use('FCCEE_P_RING')


def run_4d_twiss(mad_instance, filename):

    mad_instance.input(
"""VOLTCA1SAVE = VOLTCA1; 
VOLTCA2SAVE = VOLTCA2; 

VOLTCA1 = 0;
VOLTCA2 = 0;""")

    twiss_df = mad_instance.twiss(sequence='FCCEE_P_RING', chrom=True, file=filename).dframe()

    mad_instance.input(
"""VOLTCA1 = VOLTCA1SAVE;
VOLTCA2 = VOLTCA2SAVE;""")
    return twiss_df


def create_and_load_error(mad_instance, error_dict, seed, cwd):

    twiss_path = str(cwd/'test.tfs')
    error_path = str(cwd/f'errors_{seed}.tfs')

    run_4d_twiss(mad_instance, twiss_path)
    mad_instance.input("SELECT, FLAG=error, CLEAR;")
    cet.main(
        errors_dict=error_dict,
        twiss_file=twiss_path,
        seed=seed,
        outputfile=error_path
        )

    mad_instance.input(f"READTABLE, file='{error_path}', table=errtab;")
    mad_instance.input("SETERR, TABLE=errtab;")
