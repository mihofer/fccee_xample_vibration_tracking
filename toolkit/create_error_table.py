
import json
from pathlib import Path
import numpy as np
import tfs
from schema import Schema, Optional, And


from generic_parser import EntryPointParameters, entrypoint
from generic_parser.entry_datatypes import get_instance_faker_meta

class PathOrStr(metaclass=get_instance_faker_meta(Path, str)):
    """A class that behaves like a Path when possible, otherwise like a string."""
    def __new__(cls, value):
        if value is None:
            return None

        if isinstance(value, str):
            value = value.strip("\'\"")  # behavior like dict-parser, IMPORTANT FOR EVERY STRING-FAKER
        return Path(value)

MAX_ORDER = 20
ORDERS = range(1, MAX_ORDER+2)
PLANES = ['X', 'Y', 'S', 'PHI', 'THETA', 'PSI']

MAGNET_SCHEMA = Schema(
    {Optional(f'd{plane}_syst', default=0.): float for plane in PLANES}|
    {Optional(f'd{plane}_rand', default=0.): And(float, lambda x: x>=0.) for plane in PLANES}|
    {Optional(f'b{order}_syst', default=0.): float for order in ORDERS}|
    {Optional(f'b{order}_rand', default=0.): And(float, lambda x: x>=0.) for order in ORDERS}|
    {Optional(f'a{order}_syst', default=0.): float for order in ORDERS}|
    {Optional(f'a{order}_rand', default=0.): And(float, lambda x: x>=0.) for order in ORDERS}
)

ERROR_SCHEMA = Schema(
    {
    "Reference_radius": And(float, lambda x: x>0.),
    str: MAGNET_SCHEMA,
    }
)

ERROR_TFS_HEADER = {
'NAME': 'EFIELD',
'TYPE': 'EFIELD',
'TITLE': 'no-title',
'ORIGIN': '3.14.15 Linux 128',
'DATE': '01/13/00',
'TIME': '25.61.61',
}

NORMAL_ERRORS = [f'K{order-1}L' for order in ORDERS]
SKEW_ERRORS = [f'K{order-1}SL' for order in ORDERS]
ALIGNMENT_ERRORS = [f'D{plane}' for plane in PLANES]

# MAD-X is very picky on columns, if out of order or missing, it will not accept the file
TFS_COLUMNS = [val for pair in zip(NORMAL_ERRORS, SKEW_ERRORS) for val in pair] + ALIGNMENT_ERRORS

# Script arguments -------------------------------------------------------------
def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="twiss_file",
        type=PathOrStr,
        help="Path to the tfs file.",
    )
    params.add_parameter(
        name="errors_file",
        type=PathOrStr,
        help="Path to json file containing definitions of errors.",
    )
    params.add_parameter(
        name="twiss_df",
        type=tfs.TfsDataFrame,
        help="twiss dataframe.",
    )
    params.add_parameter(
        name="errors_dict",
        type=dict,
        help="Dictionary containing definitions of errors.",
    )
    params.add_parameter(
        name="seed",
        type=int,
        default=1,
        help="Set seed for random number generator.",
    )
    params.add_parameter(
        name="outputfile",
        type=PathOrStr,
        default=None,
        help="Path to where error tfs should be stored.",
    )
    params.add_parameter(
        name="full_table",
        type=bool,
        default=False,
        help="Flag whether all elements from the twiss are stored in the error file, including those without errors. Default false safes only elements with errors.",
    )
    return params


# Entrypoint -------------------------------------------------------------------
@entrypoint(get_params(), strict=True)
def main(opt):

    check_opts(opt)
    elements_df = get_elements(opt)
    errors = get_errors(opt)
    reference_radius = errors.pop('Reference_radius')
    random_generator = np.random.default_rng(seed=opt['seed'])

    error_df = tfs.TfsDataFrame(
        index=elements_df.index,
        headers=ERROR_TFS_HEADER,
        columns=TFS_COLUMNS,
        data=np.zeros((len(elements_df.index), len(TFS_COLUMNS))))

    error_df = assign_errors(error_df, errors, elements_df, reference_radius, random_generator)

    if not opt["full_table"]:
        error_df=error_df.loc[error_df.index.str.contains('|'.join([element for element in errors.keys()]))]

    if opt["outputfile"] is not None:
        tfs.write(opt["outputfile"], error_df, save_index='NAME')

    return error_df


def assign_errors(error_df, errors, elements_df, rref, rng):
    for key, item in errors.items():

        selected_magnets = error_df.loc[error_df.index.str.contains(f'{key}')].copy()
        strengths = elements_df.loc[elements_df.index.str.contains(f'{key}')].copy()

        if selected_magnets.empty:
            print(f'No element fitting naming {key} was found in the twiss file. Skipping.')
            continue

        # if thin elements are found, create temp. dataframe with only the main marker as elements
        # then assign random numbers for each component and assign those also to the indiv. slices
        if not selected_magnets.loc[selected_magnets.index.str.contains(f'{key}.*\\.\\.\\d*$')].empty:
            print(f'Thin elements for {key} found, using same random number for all slices.')
            main_elements = strengths.loc[~strengths.index.str.contains(f'{key}.*\\.\\.\\d*$')].copy()
            main_elements = assign_random_numbers(main_elements, rng)
            
            for idx, data in main_elements.iterrows():
                cols= data.index[data.index.str.contains('^rand_.*')]
                strengths.loc[
                    strengths.index.str.contains(f'{idx}\\.\\.\\d*$'), cols] = data[cols].to_numpy()
                strengths.loc[
                    strengths.index.str.contains(f'{idx}$'), cols] = data[cols].to_numpy()
        else:
            strengths = assign_random_numbers(strengths, rng)

        strengths['MAIN_FIELD'] = strengths.apply(select_main_field, axis=1)
        strengths['MAIN_ORDER'] = strengths.apply(select_main_order, axis=1)

        for plane in PLANES:
            selected_magnets[f'D{plane}']=strengths.apply(lambda row: add_align_error(
                                                                    row,
                                                                    item,
                                                                    plane), axis=1)

        for order in ORDERS:
            selected_magnets[f'K{order-1}L']=strengths.apply(lambda row: add_field_error(
                                                                    row,
                                                                    item,
                                                                    order,
                                                                    rref,
                                                                    'b'), axis=1)
            selected_magnets[f'K{order-1}SL']=strengths.apply(lambda row: add_field_error(
                                                                    row,
                                                                    item,
                                                                    order,
                                                                    rref,
                                                                    'a'), axis=1)

        error_df.update(selected_magnets)

    return error_df


def assign_random_numbers(df, rng):
    for plane in PLANES:
        df[f"rand_d{plane}"] = df.apply(lambda x: rng.normal(loc=0.0, scale=1.0), axis=1)
    for order in ORDERS:
        df[f"rand_b{order}"] = df.apply(lambda x: rng.normal(loc=0.0, scale=1.0), axis=1)
        df[f"rand_a{order}"] = df.apply(lambda x: rng.normal(loc=0.0, scale=1.0), axis=1)
    return df


def select_main_field(df):
    if (df['KEYWORD'] == 'SBEND') or (df['KEYWORD'] == 'RBEND'):
        return df['K0L']
    if df['KEYWORD'] == 'QUADRUPOLE':
        return df['K1L']
    if df['KEYWORD'] == 'SEXTUPOLE':
        return df['K2L']
    for order in (list(ORDERS) + ['all_zero']):
        if order == "all_zero":
            return 0
        if df[f'K{order-1}L'] > 0.0:
            return df[f'K{order-1}L']


def select_main_order(df):
    if (df['KEYWORD'] == 'SBEND') or (df['KEYWORD'] == 'RBEND'):
        return 1
    if df['KEYWORD'] == 'QUADRUPOLE':
        return 2
    if df['KEYWORD'] == 'SEXTUPOLE':
        return 3
    for order in (list(ORDERS) + ['all_zero']):
        if order == "all_zero":
            return 1
        if df[f'K{order-1}L'] > 0.0:
            return order


def add_align_error(row, errors, plane):
    return errors[f'd{plane}_syst'] + row[f'rand_d{plane}'] * errors[f'd{plane}_rand']


def add_field_error(row, errors, order,  rref, error_type):
    # use formulas from 
    # LHC-Project Report 501 https://cds.cern.ch/record/522049/files/lhc-project-report-501.pdf
    # and MAD-X user guide
    rel_error = errors[f'{error_type}{order}_syst'] + row[f'rand_{error_type}{order}'] * errors[f'{error_type}{order}_rand']
    main_order = row['MAIN_ORDER']
    main_strength = row['MAIN_FIELD']
    reference_radius_term = rref**((main_order-1) - (order-1))
    factorital_term = np.math.factorial(order-1)/np.math.factorial(main_order-1)
    return rel_error * main_strength * reference_radius_term * factorital_term


def check_opts(opt):

    if opt['twiss_file'] is not None and opt["twiss_df"] is not None:
        raise ValueError("Either `twiss_file` or `twiss_df` have to be provided, not both.")

    if opt['twiss_file'] is None and opt["twiss_df"] is None:
        raise ValueError("Neither `twiss_file` or `twiss_df` have been provided, one of them is required.")

    if opt['errors_file'] is not None and opt["errors_dict"] is not None:
        raise ValueError("Either `errors_file` or `errors_dict` have to be provided, not both.")

    if opt['errors_file'] is None and opt["errors_dict"] is None:
        raise ValueError("Neither `errors_file` or `errors_dict` have been provided, one of them is required.")


def get_elements(opt):

    if opt["twiss_df"] is not None:
        return opt["twiss_df"]
    return tfs.read(opt['twiss_file'], index='NAME')


def get_errors(opt):

    if opt["errors_dict"] is not None:
        return ERROR_SCHEMA.validate(opt["errors_dict"])
    return ERROR_SCHEMA.validate(json.load(open(opt["errors_file"], encoding='utf=8')))


# Script Mode ------------------------------------------------------------------
if __name__ == "__main__":
    main()
