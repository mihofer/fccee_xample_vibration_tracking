# FCCee Xample study

This repository contains an example how to set up an FCC-ee lattice for tracking in XSuite with turn dependent quadrupole misalignment.

## Getting started

First, a Python 3.9 environment is required.
Then install the required python packages by running 

`python -m pip install -r python_requirements.txt`.

This will install the Xsuite and cpymad packages, together with some small other packages to run the example and tests.

## Run example

To run and play with the example, run 

`jupyter notebook`

to open a jupyter instance. 

Alternatively, the script can be run on SWAN, provided a CERN account.

<a href="https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/mihofer/fccee_xample_vibration_tracking.git" target="_blank">
  <img alt="" src="https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png">
</a>

Then open the `run_tracking.ipynb` example notebook to see how to set up a study.

A number of unit tests are provided to check that any changes in the code do not break the expected behaviour.
They are located in the 'tests' directory and can be run using

`python -m pytest ./tests`