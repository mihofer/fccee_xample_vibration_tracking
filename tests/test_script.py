
import json
from pathlib import Path
import pandas as pd
import tempfile
from cpymad.madx import Madx

import xtrack as xt
import xpart as xp
import xobjects as xo

import pytest
import toolkit.create_error_table as cet
import toolkit.cpymad_utils as cpu

REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
REFERENCE_FILE = json.load(open(REPOSITORY_TOP_LEVEL/"reference_parameters.json", encoding='utf-8'))

ERROR_DICT={
    'alignment':{
        'Reference_radius': 0.01,
        'S[DF].*':{
        'dX_rand':7e-6,
        'dY_rand':20e-6,
        },
    },
    'fielderror':{
        'Reference_radius': 0.01,
        'QY.*':{
        'b3_rand':7e-6,
        },
    },
}


if __name__ == "__main__":
    errors=ERROR_DICT['alignment']
    operation_mode='t'
    with tempfile.TemporaryDirectory() as temp_dir, Madx(command_log=f"log_{errors}.madx", cwd=temp_dir) as madx:
        cpu.set_up_lattice(madx,
                           REFERENCE_FILE[operation_mode],
                           str(REPOSITORY_TOP_LEVEL/'lattices'/operation_mode/f"fccee_{operation_mode}.seq"))


        madx.input(
            """
            SELECT, FLAG=makethin, CLASS=RFCAVITY, SLICE = 1;
            SELECT, FLAG=makethin, CLASS=rbend, SLICE = 4;
            SELECT, FLAG=makethin, CLASS=quadrupole, SLICE = 4;
            SELECT, FLAG=makethin, CLASS=sextupole, SLICE = 4;
                    
            SELECT, FLAG=makethin, PATTERN="QC*", SLICE=20;
            SELECT, FLAG=makethin, PATTERN="SY*", SLICE=20;
                    
            MAKETHIN, SEQUENCE=FCCEE_P_RING, STYLE=TEAPOT, MAKEDIPEDGE=True;

            USE, SEQUENCE = FCCEE_P_RING;
        """)

        cpu.create_and_load_error(madx, ERROR_DICT[errors], 1)

        cpymad_df = madx.twiss(sequence='FCCEE_P_RING', chrom=True).dframe()

        line = xt.Line.from_madx_sequence(madx.sequence['FCCEE_P_RING'],
                                    apply_madx_errors=True,
                                    exact_drift=False, # enabled as part of tracker in the tracking scripts
                                    install_apertures=False)
    context = xo.ContextCpu()
    ref_particle = xp.Particles(mass0=xp.ELECTRON_MASS_EV, q0=1, p0c=REFERENCE_FILE[operation_mode]['ENERGY']*10**9, x=0, y=0)
    line.particle_ref = ref_particle
    tracker = xt.Tracker(_context=context, line=line)
    twiss = tracker.twiss(method="4d")
    xsuite_df = twiss.to_pandas()