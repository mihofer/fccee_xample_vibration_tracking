import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--operation_mode", action="store", default="z", choices=['z','w','h','t'], help="Define which operation mode should be tested: z,w,h,t"
    )


@pytest.fixture
def operation_mode(request):
    return request.config.getoption("--operation_mode")