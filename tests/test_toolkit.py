import numpy as np
import pandas as pd

import pytest
import tfs
import toolkit.create_error_table as cet

def test_thick(_twiss_df, _error_set):
    err_df = cet.main(twiss_df=_twiss_df, errors_dict= _error_set, seed=1)
    pd.testing.assert_frame_equal(
        err_df.loc[:, ['DX', 'DY', 'K0L', 'K1L', 'K2L']],
        pd.DataFrame(
            index=err_df.index,
            data={
            	    'DX': [0.001, 0.001, 0.001, 0.002, 0.001096],
            	    'DY': [0.001330, -0.000303, 0.001, 0.002, 0.001],
            	    'K0L': [1e-6, 1e-6, 1e-8, 1.637751e-08, 5e-11],
            	    'K1L': [1e-4, 1e-4, 1e-6, 1e-6, 5e-9],
            	    'K2L': [0.0, 0.0, 0.0, 0.0, 0.0],
                    }),
        check_exact=False,
        rtol=1e-3
        )


def test_thin(_twiss_thin_df, _error_set):
    err_df = cet.main(twiss_df=_twiss_thin_df, errors_dict= _error_set, seed=1)
    pd.testing.assert_frame_equal(
        err_df.loc[:, ['DX', 'DY', 'K0L', 'K1L', 'K2L']],
        pd.DataFrame(
            index=err_df.index,
            data={
            	    'DX': [
        0.001, 0.001, 0.001,
        0.001, 0.001, 0.001,
        0.001, 0.001, 0.001,
        0.002, 0.002, 0.002,
        0.001096, 0.001096, 0.001096],
            	    'DY': [
        0.001330, 0.001330, 0.001330,
        -0.000303, -0.000303, -0.000303,
        0.001, 0.001, 0.001,
        0.002, 0.002, 0.002,
        0.001, 0.001, 0.001],
            	    'K0L': [
        1e-6, 0.000, 1e-6,
        1e-6, 0.000, 1e-6,
        1e-8, 0.000, 1e-8,
        1e-8, 0.000, 1e-8,
        5e-11, 0.000, 5e-11],
            	    'K1L': [
        1e-4, 0.000, 1e-4,
        1e-4, 0.000, 1e-4,
        1e-6, 0.000, 1e-6,
        1e-6, 0.000, 1e-6,
        5e-9, 0.000, 5e-9],
            	    'K2L': [
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000],
                    }),
        check_exact=False,
        rtol=1e-3
        )


def test_thick_short_table(_twiss_df, _error_set):
    err_df = cet.main(twiss_df=_twiss_df, errors_dict= {key: item for key, item in _error_set.items() if key!='DIP.*'}, seed=1)
    assert all(err_df.index == ['QUAD1', 'QUAD2', 'SEXT1'])


def test_thin_short_table(_twiss_thin_df, _error_set):
    err_df = cet.main(twiss_df=_twiss_thin_df, errors_dict= {key: item for key, item in _error_set.items() if key!='DIP.*'}, seed=1)
    assert all(err_df.index == ['QUAD1..1', 'QUAD1', 'QUAD1..2', 'QUAD2..1', 'QUAD2', 'QUAD2..2', 'SEXT1..1', 'SEXT1', 'SEXT1..2'])


@pytest.fixture()
def _twiss_df():
    return tfs.TfsDataFrame(
    data={
        'KEYWORD': ['SBEND', 'SBEND', 'QUADRUPOLE', 'QUADRUPOLE', 'SEXTUPOLE'],
        'K0L': [0.001, 0.001, 0.000, 0.000, 0.000],
        'K1L': [0.000, 0.000, 0.001, 0.001, 0.000],
        'K2L': [0.000, 0.000, 0.000, 0.000, 0.001],
    },
    index=['DIP1', 'DIP2', 'QUAD1', 'QUAD2', 'SEXT1']
    )


@pytest.fixture()
def _twiss_thin_df():
    return tfs.TfsDataFrame(
    data=
    {f'K{order-1}L': np.zeros(15) for order in cet.ORDERS}|
    {
        'KEYWORD': [
        'MULTIPOLE', 'MARKER', 'MULTIPOLE',
        'MULTIPOLE', 'MARKER', 'MULTIPOLE',
        'MULTIPOLE', 'MARKER', 'MULTIPOLE',
        'MULTIPOLE', 'MARKER', 'MULTIPOLE',
        'MULTIPOLE', 'MARKER', 'MULTIPOLE'],
        'K0L': [
        0.001, 0.000, 0.001,
        0.001, 0.000, 0.001,
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000],
        'K1L': [
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000,
        0.001, 0.000, 0.001,
        0.001, 0.000, 0.001,
        0.000, 0.000, 0.000],
        'K2L': [
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000,
        0.000, 0.000, 0.000,
        0.001, 0.000, 0.001],
    },
    index=[
        'DIP1..1', 'DIP1', 'DIP1..2',
        'DIP2..1', 'DIP2', 'DIP2..2',
        'QUAD1..1', 'QUAD1', 'QUAD1..2',
        'QUAD2..1', 'QUAD2', 'QUAD2..2',
        'SEXT1..1', 'SEXT1', 'SEXT1..2']
    )


@pytest.fixture()
def _error_set():
    return {
        'Reference_radius': 0.01,
        'DIP.*':{
        'dX_syst':0.001,
        'dY_syst':0.001,
        'dY_rand':0.001,
        'b1_syst':0.001,
        'b2_syst':0.001,
        },
        'QUAD.*':{
        'dX_syst':0.001,
        'dY_syst':0.001,
        'b1_syst':0.001,
        'b2_syst':0.001,
        },
        'QUAD2':{
        'dX_syst':0.002,
        'dY_syst':0.002,
        'b1_syst':0.001,
        'b1_rand':0.001,
        'b2_syst':0.001,
        },
        'QUAD.2':{
        'dX_syst':0.001,
        'dY_syst':0.001,
        'b1_syst':0.001,
        'b1_rand':0.001,
        'b2_syst':0.001,
        },
        'SEXT.*':{
        'dX_syst':0.001,
        'dX_rand':0.001,
        'dY_syst':0.001,
        'b1_syst':0.001,
        'b2_syst':0.001,
        },
        }
